import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterexchanger/Currency.dart';
import 'package:flutterexchanger/ImageReader.dart';
import 'package:flutterexchanger/MyListView.dart';
import 'package:flutterexchanger/main.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:string_validator/string_validator.dart';

class View extends StatefulWidget {
  @override
  _ViewState createState() => _ViewState();
}

class _ViewState extends State<View> {
  final globalKey = GlobalKey<ScaffoldState>();
  MyListView myListView = new MyListView();
  final AppModel appModel = AppModel();

  @override
  void initState() {
    jsonParser(appModel);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: globalKey,
        appBar: AppBar(title: Text("Money Exchanger")),
        floatingActionButton: ScopedModelDescendant<AppModel>(
            builder: (context, child, model) => Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FloatingActionButton(
                        child: Icon(Icons.account_balance),
                        onPressed: () { jsonParser(model);}),
                    FloatingActionButton(
                        child: Icon(Icons.add),
                        onPressed: () {
                          _readInputText(context, model);
                        }),
                  ],
                )),
        body: ScopedModelDescendant<AppModel>(
            builder: (context, child, model) => Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("images/money.jpeg"),
                              fit: BoxFit.cover)),
                    ),
                    SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          children: <Widget>[
                            SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: createTable(model.curr)),
                            ImageReader(),
                          ],
                        )),
                  ],
                )));
  }

  jsonParser(AppModel curr) async {
    final response = await http.get(
        "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5");
    if (response.statusCode == 200) {
      print("response 200");
      var allData = json.decode(response.body) as List<dynamic>;
      allData.forEach((object) {
        Currency currency = new Currency(object["ccy"], double.parse(object["buy"]),
            double.parse(object["sale"]), 1);
        if (currency.name != "BTC") {
          print("dodaem");
          curr.increment(currency);}
      });
    } else {
      var snack = SnackBar(
        content: Text("cant load data from internet"),
      );
      globalKey.currentState.showSnackBar(snack);
    }
  }

  _readInputText(BuildContext context, AppModel curr) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Enter your currency: "),
            content: Row(
              children: <Widget>[myListView],
            ),
            actions: <Widget>[
              MaterialButton(
                child: Text("OK"),
                onPressed: () {
                  setState(() {
                    curr.increment(Currency(
                        myListView.currController.text,
                        double.parse(myListView.buyController.text),
                        double.parse(myListView.sellController.text),
                        1));
                  });
                  Navigator.pop(context);
                  myListView.currController.clear();
                  myListView.buyController.clear();
                  myListView.sellController.clear();
                },
              )
            ],
          );
        });
  }

  Widget createTable(List<Currency> curr) {
    return DataTable(
      horizontalMargin: 20,
      columnSpacing: 10,
      columns: [
        DataColumn(
          label: Expanded(
              child: Text(
            "CURRENCY",
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
          )),
          numeric: false,
        ),
        DataColumn(
          label: Text("BUY",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          numeric: false,
        ),
        DataColumn(
          label: Text("SELL",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          numeric: false,
        ),
        DataColumn(
          label: Text("SUM",
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          numeric: false,
        ),
        DataColumn(label: Text(""))
      ],
      rows: curr
          .map((currency) => DataRow(cells: [
                DataCell(Text(currency.name,
                    style: TextStyle(fontSize: 20, color: Colors.white))),
                DataCell(Center(
                    child: TextField(
                  onChanged: (text) {
                    currency.buy = double.parse(text);
                  },
                  keyboardType: TextInputType.number,
                  controller: TextEditingController(
                      text: (currency.buy * currency.quantity).toString()),
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ))),
                DataCell(Center(
                    child: TextField(
                        onChanged: (text) {
                          currency.sell = double.parse(text);
                        },
                        keyboardType: TextInputType.number,
                        controller: TextEditingController(
                            text:
                                (currency.sell * currency.quantity).toString()),
                        style: TextStyle(fontSize: 20, color: Colors.white)))),
                DataCell(Center(
                    child: TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      hintText: "1",
                      hintStyle: TextStyle(color: Colors.white70)),
                  style: TextStyle(fontSize: 20, color: Colors.white),
                  onChanged: (text) {
                    setState(() {
                      currency.quantity = double.parse(text);
                    });
                  },
                ))),
                DataCell(Center(
                  child: IconButton(
                    icon: Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      setState(() {
                        curr.remove(currency);
                      });
                    },
                  ),
                )),
              ]))
          .toList(),
    );
  }
}
