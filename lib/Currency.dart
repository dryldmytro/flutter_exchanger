class Currency{
  String name;
  double buy;
  double sell;
  double quantity;

  Currency(this.name, this.buy, this.sell, this.quantity);

  @override
  String toString() {
    return 'Currency{name: $name, buy: $buy, sell: $sell, quantity: $quantity}';
  }

}