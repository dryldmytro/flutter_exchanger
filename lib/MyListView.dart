
import 'package:flutter/material.dart';

class MyListView extends StatefulWidget {
  final currController = TextEditingController();
  final buyController = TextEditingController();
  final sellController = TextEditingController();

  @override
  _MyListViewState createState() => _MyListViewState();
}

class _MyListViewState extends State<MyListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      padding: EdgeInsets.all(5),
      child: new Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: widget.currController,
              decoration: InputDecoration(
                  hintText: "currency",
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black))),
            ),
          ),
          Expanded(
            child: TextField(
              keyboardType: TextInputType.number,
              controller: widget.buyController,
              decoration: InputDecoration(
                  hintText: "buy",
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black))),
            ),
          ),
          Expanded(
            child: TextField(
              keyboardType: TextInputType.number,
              controller: widget.sellController,
              decoration: InputDecoration(
                  hintText: "sell",
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black))),
            ),
          ),
        ],
      ),
    );
  }
}
