import 'dart:io';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutterexchanger/main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:string_validator/string_validator.dart';

import 'Currency.dart';

class ImageReader extends StatefulWidget {
  @override
  _ImageReaderState createState() => _ImageReaderState();
}

class _ImageReaderState extends State<ImageReader> {
  File pickedImage;
  var text = "";
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
      builder: (context,child,model)=>getDataFromGallery(model),
    );
  }

  Widget getDataFromGallery(AppModel curr) {
    return Container(
        child: Column(
          children: <Widget>[
            showText(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: IconButton(
                    icon: Icon(Icons.add_photo_alternate),
                    onPressed: () {
                      pickImageFromGallery(ImageSource.gallery);
                    },
                  ),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.file_upload),
                    onPressed: () {
                      parseString(text, curr);
                    },
                  ),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(Icons.clear),
                    onPressed: () {
                      setState(() {
                        text = "";
                      });
                    },
                  ),
                )
              ],
            )
          ],
        ));
  }

  pickImageFromGallery(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);
    setState(() {
      pickedImage = image;
    });
    getText(pickedImage);
  }

  Widget showText() {
    if (pickedImage != null && text != "") {
      return Center(
        child: Container(
          width: 200,
          height: 150,
          color: Colors.white,
          child: TextField(
            maxLines: null,
            onChanged: (t) {
              text = t;
            },
            controller: TextEditingController(
              text: text,
            ),
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ),
      );
    } else
      return Container();
  }

  void getText(File pickedImage) async {
    if (pickedImage != null) {
      FirebaseVisionImage visionImage =
      FirebaseVisionImage.fromFile(pickedImage);
      TextRecognizer textRecognizer = FirebaseVision.instance.textRecognizer();
      VisionText visionText = await textRecognizer.processImage(visionImage);
      for (TextBlock block in visionText.blocks) {
        for (TextLine line in block.lines) {
          for (TextElement word in line.elements) {
            setState(() {
              text = text + word.text + " ";
            });
          }
          text = text + "\n";
        }
      }
      textRecognizer.close();
    }
  }

  void parseString(String text, AppModel curr) {
    List<String> courses = text.split("\n");
    for (String s in courses) {
      String string = s.replaceAll(" ", "|").replaceAll(",", ".");
      List letters = [];
      List symbols = [];
      for (var ch in string.runes) {
        String s = String.fromCharCode(ch);
        if (isAlpha(s)) {
          letters.add(s);
        } else
          symbols.add(s);
      }
      String currencyName = letters
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "")
          .replaceAll(",", "")
          .replaceAll(" ", "");
      var currenciesValues = symbols
          .toString()
          .replaceAll("[", "")
          .replaceAll("]", "")
          .replaceAll(",", "")
          .replaceAll(" ", "")
          .split("|");
      List doubles = [];
      for (String s in currenciesValues) {
        if (isFloat(s) && s.length > 0) {
          doubles.add(s);
        }
      }
      if (currencyName.length > 0) {
        Currency currency = new Currency(currencyName, double.parse(doubles[0]),
            double.parse(doubles[1]), 1);
        setState(() {
          curr.increment(currency);
        });
      }
    }
  }
}
