import 'package:flutter/material.dart';
import 'package:flutterexchanger/view.dart';
import 'package:scoped_model/scoped_model.dart';

import 'Currency.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Directionality(
        textDirection: TextDirection.ltr,
        child: ScopedModel<AppModel>(
          model: AppModel(),
          child: View(),
        )
      ),
    );
  }
}


class AppModel extends Model{
  List<Currency> _curr = [];

  List<Currency> get curr => _curr;

  void increment(Currency currency){
    _curr.add(currency);
    notifyListeners();
  }

}
